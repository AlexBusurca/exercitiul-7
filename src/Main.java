public class Main {
    public static void main(String[] args) {
        String[] receptionists = new String[5];
        int[]  floors= new int[5];
        receptionists[0] = "John Snow";
        receptionists[1] = "William Cooper";
        receptionists[2] = "Patricia Johnson";
        receptionists[3] = "Kate Anderson";
        receptionists[4] = "Dustin Rhodes";
        floors[0] = 1;
        floors[1] = 2;
        floors[2] = 3;
        floors[3] = 4;
        floors[4] = 5;

        System.out.println("The receptionist on the " + floors[0] + "st floor is: " + receptionists[0] + ",\n"+
                           "The receptionist on the " + floors[1] + "nd floor is: " + receptionists[1] + ",\n"+
                           "The receptionist on the " + floors[2] + "rd floor is: " + receptionists[2] + ",\n"+
                           "The receptionist on the " + floors[3] + "th floor is: " + receptionists[3] + ",\n"+
                           "The receptionist on the " + floors[4] + "th floor is: " + receptionists[4] +".");

            String[][] Owners = new String[2][2];
            Owners[0][0] = "Mr.";
            Owners[0][1] = "Ms.";
            Owners[1][0] = "Smith Riley";
            Owners[1][1] = "Catherine Jade";

        System.out.println(Owners[0][0] +" "+ Owners[1][0] + " and " + Owners[0][1] + " " + Owners[1][1] + " are the owners of the Plaza Hotel.");

        char [] elements = {'B','E','D','O','N','A','L','D','D','A','T','E'};
        char [] result = new char[6];


        System.arraycopy(elements,2,result,0,6);
        String name = new String(result);

        System.out.println("The hotel's security is provided by: " + result[0] + result[1] + result[2] + result[3] + result[4] + result[5] );
        System.out.println("The hotel's security is provided by: " + name );



    }
}
